import React from 'react';
import SignIn from '../components/SignIn';

const Home = () => {
    return (
        <div className="row mt-5">
            <div className="col">
                <SignIn />
            </div>
        </div>
    )
}

export default Home;