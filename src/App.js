import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './views/Home';
import NoMatch from './views/NoMatch';
import Header from './components/Header';
import Footer from './components/Footer';
import Layout from './components/Layout';

const App = () => (
  <Router>
    <Header />
    <Layout>
      <Switch>
        <Route exact path="/" component={ Home } />
        <Route path="/NoMatch" component={ NoMatch } />
      </Switch>
    </Layout>
    <Footer />
  </Router>
)

export default App;
