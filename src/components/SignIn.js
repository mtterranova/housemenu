import React, { useState } from 'react';
import axios from 'axios';

const SignIn = () => {
    const [ email, setEmail ] = useState("");
    const [ password, setPassword ] = useState("");

    const handleEmail = e => {
        if (e.key === "Enter") {
            handleSubmit(email, password);
        }
        setEmail(e.target.value)
    }

    const handlePassword = e => {
        if (e.key === "Enter") {
            handleSubmit(email, password);
        }
        setPassword(e.target.value);
    }

    const handleSubmit = () => {
        // Handle form validation
        console.log({email, password})
        axios.post('/signin', { email, password })
        .then(res => console.log(res))
        .catch(err => console.log(err))
    }

    return (
        <div style={{display: 'flex', justifyContent: 'center'}}>
            <div className="text-center" style={{ width: '80%' }}>
                <div className="form-signin">
                    <h1 className="h3 mb-3 font-weight-normal">Please Sign in</h1>
                    <input type="email" className="form-control" placeholder="Email address" onChange={handleEmail} onKeyPress={handleEmail} required autoFocus />
                    <input type="password" className="form-control" placeholder="Password" onChange={handlePassword} onKeyPress={handlePassword} required />
                    <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                </div>
            </div>
        </div>
    )
}

export default SignIn
