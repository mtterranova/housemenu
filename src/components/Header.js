import React from 'react';

const Header = () => (
    <header className="header">
        <div className="container">
            <div className="row">
                <div className="col mt-4">
                    <h2 style={{ color: 'white' }}>House Menu</h2>
                </div>
            </div>
        </div>
    </header>
)

export default Header;