const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const app = express();

app.use(express.static(path.join(__dirname, 'build')));
const corsOptions = {
    origin: "http://localhost:8000"
};
// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(corsOptions));

const db = require("./models");
const Role = db.role;

db.sequelize.sync({force: true}).then(() => {
    console.log("Drop and Resync DB");
    initial();
})

function initial() {
    Role.create({
        id: 1,
        name: "user"
    });

    Role.create({
        id: 2,
        name: "admin"
    })
}

// routes
require('./routes/auth.routes')(app);
require('./routes/user.routes')(app);

app.post('/signin', function (req, res) {
    res.send({ user: req.body.email, password: req.body.password });
});

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

// set port, listen for requests
const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
